# Dictionary defining instruction names and their opcodes
instr_dict = {
"PRT_C":254,
"PRT_N":225,
"SET_VMODE":224,
"SET_CX":223,
"SET_CY":222,
"SET_CF":221,
"SET_CUR":220,
"KB_POP":219,
"KB_PUSH":218,
"JMP":252,
"JME":250,
"JMD":249,
"JMB":248,
"JMS":247,
"JMN":246,
"JMM":245,
"JMP_R":244,
"JME_R":243,
"JMD_R":242,
"JMB_R":241,
"JMS_R":240,
"JMN_R":239,
"SET":237,
"XCH":236,
"MOV":235,
"MOVR":234,
"MOVM":233,
"INCR":217,
"DECR":216,
"ADD":232,
"SUB":231,
"MUL":230,
"DIV":229,
"AND":228,
"NOT":227,
"OR":226,
"HLT":255,
"INT":215,
"RET":214,
"SCR_CLR":213,
"SCR_SHIFT":211, # TODO
"GET_CUR":212,
"XOR":210,
"MOD":209,
"SLEEP":208
}

# Dictionary defining register aliases and their numeric ID
register_dicts = {
"G0":0,
"G1":1,
"G2":2,
"G3":3,
"G4":4,
"G5":5,
"G6":6,
"G7":7,
"G8":8,
"G9":9,
"GA":10,
"GB":11,
"GC":12,
"GD":13,
"GE":14,
"GF":15,
"I0":16,
"I1":17,
"I2":18,
"I3":19,
"I4":20,
"I5":21,
"I6":22,
"I7":23,
"I8":24,
"I9":25,
"IA":26,
"IB":27,
"IC":28,
"ID":29,
"IE":30,
"IF":31,
"IP":32,
"RET":31,
"JM":15
}

# Import necesarry stuff
import numpy, sys

# Define this for an easier life
def file_get_name():
    import Tkinter, tkFileDialog

    root = Tkinter.Tk()
    root.withdraw()

    file_path = tkFileDialog.askopenfilename()
    return file_path

def is_number(s):
    try:
        float(s)
        return True
    except:
        return False

print "CHIP ARBITRARY - Assembler - Made By Re - Version 1.1"
print "---------------------------------------"

# Missing SRC and DEST?
if len(sys.argv) == 1:
    print "No source or destination file specified"
    print "Usage: ca_assemble <source asm file> OPTIONAL <destination file>"
    print "Example: ca_assemble fibbonaci.asm fibbonaci.txt"
    print "Press any key to continue"
    raw_input()
    sys.exit()

# Missing just SRC?
if len(sys.argv) == 2:
    src_file = sys.argv[1]
    dst_file = "out.txt"
else:
    src_file = sys.argv[1]
    dst_file = sys.argv[2]


# Attempt to open it
try:
    file = open(src_file, 'r')
except:
    print("Source file not found")
    sys.exit()
program_txt = file.read() # load its full contents to a variable
print "Building "+src_file
program_instructions = program_txt.split("\n") # Separate our string by the newlines into a tuple/list

processed_instructions = []

for rawinstr in program_instructions: # Loop though each line, extract all comments
    instr=  rawinstr.split(";")[0].strip()
    if instr == "":
        continue
    processed_instructions.append(instr)

# Finished extracting comments

#print "Filtered Lines: "
#print processed_instructions

# Now to actually assemble the instruction
processed_bytecode = []

# Define a dict of all labels/positions
labels = {}
data_sets = {}

# First pass, Assembling (Do not fill labels)
for instr in processed_instructions:
    instr = instr.split(" ") # Split it by the spaces

    # First, Determine if this is a label
    if ":" in instr[0]:
        # This is a label?

        # Check if this is a DATA label
        if instr[0].split(":")[0].upper() == "DATA":
            # Make sure this data's identifer name doesn't already exist
            if data_sets.has_key(instr[0].split(":")[1].upper() ):
                print "ASSEMBLING ERROR"
                print "Data Names MUST be unique"
                print "Offending name: "+instr[0].split(":")[1]
                sys.exit()
            # Data label syntax
            # DATA:<Desired Reference Name>:"STRING"
            full_str =  " ".join(instr)
            data_sets[full_str.split(":")[1].upper() ] = full_str.split(":",2)[2]
            continue # End here for this iteration

        # Make sure this label doesn't already exist
        if labels.has_key(instr[0].split(":")[0].upper() ):
            print "ASSEMBLING ERROR"
            print "Labels MUST be unique"
            sys.exit()

        # If we are here, label is new/unique so we can add it
        labels[instr[0].split(":")[0].upper() ] = len(processed_bytecode) # Add this to our dict
        #print labels
        continue

    # Now,
    try:
        opcode = instr_dict[instr[0].upper() ] # See if the instruction is valid
        processed_bytecode.append(opcode)
    except:
        print "ASSEMBLING ERROR"
        print "Unknown Instruction "+instr[0]
        sys.exit()

    for params in instr[1:]: # Loop through only the parameters now.
        # Don't look up the register alias if we're parsing a SET instruction's value, as the value is always direct and not from register
        if processed_bytecode[len(processed_bytecode)-2] == 237 and len(processed_bytecode) != 1:
            param_code = params
        elif processed_bytecode[len(processed_bytecode)-1] == 215:
            param_code = params
        else:
            try:
                param_code = register_dicts[params.upper()]
            except:
                print "ASSEMBLING ERROR"
                print "Unknown Register "+params
                print "HINT: If jumping to <LABEL>"
                print "HINT: Set a register to <LABLE>, and use the register"
                print "HINT: Registers must be written down as R0-R31 or IP"
                sys.exit()
        processed_bytecode.append(param_code)

print "OK"

print "---------------------------------------"
print "Loading Datasets"
data_sets_pos = {}
for datakey in data_sets:
    data_sets_pos[datakey] = len(processed_bytecode)
    for char in data_sets[datakey][1:-1]:
        processed_bytecode.append(ord(char))
    processed_bytecode.append(3) # ASCII code 3 for end of line

print "OK"

print "---------------------------------------"
print "Filling aliases (labels & dataset reference)"
# Second pass, filling labels/Dataset Labels
for x in range(len(processed_bytecode)):
    # Fill a label?
    if labels.has_key(processed_bytecode[x]):
        processed_bytecode[x] = labels[processed_bytecode[x]]

    # Fill a dataset label?
    if type(processed_bytecode[x]) == type(""):
        if processed_bytecode[x].split(":")[0].upper()  == "DATA":
            #print "REFERENCE TO DATASET "+processed_bytecode[x]
            processed_bytecode[x] = data_sets_pos[processed_bytecode[x].split(":")[1].upper() ]
print "OK"
print "---------------------------------------"
print "Complete."
print "---------------------------------------"
print "Assembled Code (Len: "+str(len(processed_bytecode))+")"
print ""
assemcode = "Hex: "
for instr in processed_bytecode:
    assemcode +=  str(hex(int(instr))[2:].upper())+" "
print assemcode
print ""
assemcode = "Dec: "
for instr in processed_bytecode:
    assemcode +=  str((int(instr)))+" "

print assemcode

print ""
print "Assembled Labels"
print labels
print ""
print "Assembled Datasets"
print data_sets
print ""
print "---------------------------------------"
print "Stats"
print str(len(program_txt.split("\n")))+" line(s)"
print str(len(processed_instructions)-len(labels)-len(data_sets))+" instruction(s)"
print str(len(labels))+" label(s)"
print str(len(data_sets))+" dataset(s)"
print "---------------------------------------"
print "Building NumPy array"
assembled_instructions = numpy.zeros(len(processed_bytecode)) # Define our numpy array
for x in range(len(processed_bytecode)):
    assembled_instructions[x] = processed_bytecode[x]
numpy.savetxt(dst_file,assembled_instructions)
print "OK"
print "---------------------------------------"
print "Build saved to "+dst_file
print "Press any key to continue"
raw_input()
