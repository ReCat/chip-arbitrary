; Re's CHIP ARBITRARY
; Fibbonaci Calculator
; Uses "Serial Console" for output
; DATE: 4/6/2014
; For version 1.0

SET R0 8 ; This will contain total iterations
SET R1 1 ; Current Number
SET R2 0 ; Last Number

LOOP_START:

ADD R1 R2 R4 ; Add the Current Number with last number, store result in R4
MOV R1 R2 ; Copy contents of current number to last number
MOV R4 R1 ; Copy contents of sum to current number

; Decrement increment counter
DECR R0

PRT_N R1 ; Print Current Number

; Print space
SET R6 32
PRT_C R6

; Jump to PROGRAM_END label If R0 == R3
SET R8 PROGRAM_END	
JME R8 R0 R3 	; R3 was never set, so it is 0 by default

; Jump to LOOP_START label
SET R8 LOOP_START	
JMP R8

PROGRAM_END:
SET R0 68
PRT_C R0
SET R0 79
PRT_C R0
SET R0 78
PRT_C R0
SET R0 69
PRT_C R0
HLT