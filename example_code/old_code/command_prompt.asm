; Re's CHIP ARBITRARY
; Dummy Command Prompt
; Keyboard Input Example
; ASM Verion 1.0

SET R0 0 ; Current line
SET R1 80 ; Line character limit
SET R2 0 ; Current Char Pos on Line
SET R3 65 ; Current Drive Letter
SET R6 95 ; Cursor character

; Prints: "ReOS"
PRINT_TITLE:
SET R4 0
SET_CX R4 ; Set cursor X to 0
SET_CY R0 ; Set cursor Y to our current line
SET R4 82
SET_CUR R4 ; Print Drive Letter to cursor

SET R4 1
SET_CX R4 ; Set cursor X to 0
SET_CY R0 ; Set cursor Y to our current line
SET R4 101
SET_CUR R4 ; Print Drive Letter to cursor

SET R4 2
SET_CX R4 ; Set cursor X to 0
SET_CY R0 ; Set cursor Y to our current line
SET R4 79
SET_CUR R4 ; Print Drive Letter to cursor

SET R4 3
SET_CX R4 ; Set cursor X to 0
SET_CY R0 ; Set cursor Y to our current line
SET R4 83
SET_CUR R4 ; Print Drive Letter to cursor

; Prints the text at the beginning of a line
PRINT_PREFIX:
; Increment Current Line
INCR R0

; DRIVE LETTER
SET R4 0
SET_CX R4 ; Set cursor X to 0
SET_CY R0 ; Set cursor Y to our current line
SET_CUR R3 ; Print Drive Letter to cursor

; SEMICOLON
SET R4 1   
SET_CX R4 ; Change cursor x to 1
SET R4 58
SET_CUR R4
; BACKSLASH

SET R4 2   
SET_CX R4 ; Change cursor x to 2
SET R4 92
SET_CUR R4

; LESSTHAN
SET R4 3
SET_CX R4 ; Change cursor x to 3
SET R4 62
SET_CUR R4

; CURSOR
SET R4 4
SET_CX R4 ; Change cursor x to 3
SET_CUR R6 ; Print cursor char

; Set start point for printing text
SET R2 4

; Polls keyboard for data and writes any letters
; If enter is pressed, go to next line 
WRITE_TEXT:
KB_POP R7 ; Pop first event to R7

SET R31 WRITE_TEXT
SET R8 -1
JME R31 R7 R8; Start over if R7 == -1 (no event)

; If we are here, there is a KBD event

; ENTER = 13
; If user pressed enter, JMP to PRINT_PREFIX
SET R30 13
SET R31 PRINT_PREFIX
JME R31 R30 R7

; If we are here, print what the user typed and increment R2
SET_CX R2
SET_CUR R7
INCR R2

SET R31 WRITE_TEXT
JMP R31

;intro_text:strCommand Prompt - V 1.0