; String Printing Example using Interrupts

; Re's Chip arbitrary
; ASM Version 1.1


START:
SET G0 DATA:WelcomeMessage ; Set the starting position to the start of the string
INT PRT_STRING ; Call our string printing interrupt
HLT

; Define our string data set
DATA:WelcomeMessage:"Welcome to Re's Chip Arbitrary!"

; String printing interrupt. Prints sting starting at G0 ending when $ is reached
; Does not change any general registers. 
; NOTE: All interrupts should be put at the end of file, so they are not executed
; Upon interpreter startup
PRT_STRING:
	; Copy the value in G0 to I0 for manipulation
	MOV G0 I0
	
	; Define a loop start
	PRT_STRING_LOOP:
	; Load the contents of memory at our current position to I1
	MOVM I1 I0

	; Print contents of I1
	PRT_C I1

	; Increment string position
	INCR I0
	
	; Load the contents of memory at our current position to I1 again to check
	; If we reached end of string
	MOVM I1 I0
	
	
	SET JM PRT_STRING_LOOP ; Set the jump position to the start
	SET I2 36 ; Set I2 to the end of string character
	JMD I1 I2 ; Jump to JM if our current character is NOT the end of string character
	; If we're here, Then we reached end of string
	RET ; Return