; BASIC NOTEPAD
; KBD Input Demo

; Re's Chip arbitrary
; ASM Version 1.1

SET G0 -1 ; This is what to compare for if there's no KBD event
; G1 reserved for KBD Char
SET G3 0 ; Screen Char pos
SET G4 0 ; Screen Line Pos
SET G5 81 ; Screen Char Per Line Limit
SET G6 13 ; Enter key code

SET JM START ; Set jump position to START, which is the only place we'll be jumping

SLEEPSTART:
	SET GE 100
	SLEEP GE
	
	START:
	
	KB_POP G1 ; Check KBD for events, send to G1
	SET JM SLEEPSTART
	JME G0 G1  ; Jump if G1 = -1 (G0's contents)
	
	; Check if we have to line wrap
	SET JM NEWLINE
	JME G3 G5 ; Spawn a newline if G3 = G5 (81)
	JME G1 G6 ; Spawn a newline if user pressed enter too
	
	SET_CX G3 ; Set the cursor X position
	SET_CUR G1 ; Print the popped event
	INCR G3 ; Increment the CHR pos
	
	; Jump to start now
	SET JM START 
	JMP
	
; Called when a newline needs to be made
NEWLINE:
	INCR G4 ; Increment our current line
	SET_CY G4 ; Set the new current line
	SET G3 0 ; Set cursor X to 0 again
	; Jump to start now
	SET JM START
	JMP