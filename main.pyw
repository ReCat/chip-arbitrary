from __future__ import print_function
import pygame, sys, time, random, numpy
# TODO: FIX STUPID INCONSISTENCY WITH WHAT SPECIFIES POSITIONS IN ALL JUMP INSTRUCTIONS

# CORE EXECUTION STUFF
memory = numpy.zeros(1024*16)
registers = numpy.zeros(33)

# DISPLAY STUFF
v_mode = 0 # Video Display Mode (Text, Raster)
# Define Virtual Display Console (Monochrome 82x25 ACII)
console_buffer = numpy.zeros(82*25).reshape(82,25)

# Specified Console Cursor.
c_x = 0
c_y = 0

# Console Cursor state
c_f = 1 # 0=on 1=blinking

console_buffer[0,0] = ord("_")

# INSTRUCTION LIST BOX
instruction_buffer = numpy.zeros(21, dtype=object)
# Blank it
for n in range(0,21):
    instruction_buffer[n] = ""

# SERIAL DATA BOX
# Define number of bytes to retain
serial_recency = 18*18
# Used for storing past 18*18 bytes
serial_buffer = numpy.zeros(serial_recency)

# KEYBOARD
# Stack for recording kbd events
kbd_events = []
# Push: kbd_events.append(KEY)
# Pop: kbd_events.pop()


def file_get_name():
    import Tkinter, tkFileDialog

    root = Tkinter.Tk()
    root.withdraw()

    file_path = tkFileDialog.askopenfilename()
    return file_path

def load_program(type="np"):
    log_add("")
    log_add("Load Program Procedure")
    # Open file dialouge, select file, and load it's program into memory
    global memory, registers, console_buffer, serial_buffer, serial_recency, c_x, c_y
    src_file = file_get_name()
    if type=="txt":
        try:
            file = open(src_file, 'r')
        except:
            print("Machine code file not found")
            sys.exit()
        program_txt = file.read() # load its full contents to RAM

        program_length = program_txt.count("\n") + program_txt.count(" ")

        program = numpy.zeros(program_length+1)
        injection_index = 0
        # first, we split it by it's newlines
        # then, by spaces
        # add all the contents to the array
        for x in range(len(program_txt.split("\n"))):
            for y in range(len(program_txt.split("\n")[x].split(" "))):
                try:
                    program[injection_index] = int(program_txt.split("\n")[x].split(" ")[y])
                except:
                    print("Error Parsing: Unexpected char at line "+str(x+1))
                    sys.exit()
                injection_index = injection_index + 1
    else:
        try:
            program = numpy.loadtxt(src_file) # Load our machine code file
        except:
            log_add("Error Parsing File")
            return 0
    memory = numpy.zeros(1024*16) # Blank out our memory
    log_add("Memory reset")
    # Dump our machine code to memory
    for n in range(0,len(program)):
        memory[n] = program[n]
    log_add("Program Loaded")

    # Reset EVERYTHING
    registers = numpy.zeros(33)
    console_buffer = numpy.zeros(82*25).reshape(82,25)
    serial_buffer = numpy.zeros(serial_recency)
    kbd_events = []
    c_x = 0
    c_y = 0
    log_add("Ready")


#load_program()

vm_state = 0 #0=ready, 1=running, 2=haulted, 3=crashed


# GUI Settings
screen_dimensions = (1280-32,700-8+24)
pygame.init()
screen = pygame.display.set_mode(screen_dimensions) #Set display mode
pygame.display.set_caption("CHIP ARBITRARY - VERSION 1.1 - BY RE")

clock = pygame.time.Clock()
#Define fonts
font = pygame.font.SysFont("consolas", 14)

# Define Colors
red = (255,128,0)
green = (0,255,0)
blue = (0,0,255)

white = (247,247,247)
black = (0,0,0)


# Timing variables
tick0 = 1 # Changes every 1 second
tick0_lasttick = int(time.time())
tick1 = 0 # Changes every 500 miliseconds
tick1_lasttick = time.time()
tick2 = 0 # Changes every 250 miliseconds
tick2_lasttick = time.time()

def kbd_event_push(event):
    global kbd_events
    kbd_events.append(event)

def kbd_event_pop():
    global kbd_events
    if kbd_events == []:
        return -1
    else:
        return kbd_events.pop(0)


def serial_add(data):
    global serial_buffer
    for x in range(len(serial_buffer)):
        if serial_buffer[x] == 0.0:
            pos = x
            break
        if x == len(serial_buffer)-1:
            serial_buffer = numpy.roll(serial_buffer,-1)
            pos = len(serial_buffer)-1
            break
    serial_buffer[pos] = ord(data)

def log_add(string):
    global instruction_buffer
    instruction_buffer = numpy.roll(instruction_buffer,-1)
    instruction_buffer[len(instruction_buffer)-1] = string

def render_button(xy, flashing, text = "", bk_col = white):
    global tick
    global screen
    x, y= xy

    pygame.draw.rect(screen,bk_col,(x, y, 96, 24), 0)
    pygame.draw.rect(screen,black,(x, y, 96, 24), 1)

    if flashing:
        if tick2:
            pygame.draw.rect(screen,red,(x+1, y+1, 94, 22), 0)

    data = text
    text = data[:9] + (data[9:] and '..')
    text = font.render(text, True, (0,0,0))
    screen.blit(text, ( x + (47-(text.get_width()/2)) , y+ (12-(text.get_height()/2)) ) )

def draw_text(xy, text, color=black):
    global screen
    try:
        text = font.render(text, True, color)
        screen.blit(text, xy)
    except:
        pass

def grid_pos(xin,yin):
    return ((xin*(96+16)+16),(yin*(24+16))+16)

def render_gui():
    global screen, instruction_count, last_ip, core_activity
    # Blank out screen.
    screen.fill((245,246,247))

    # Render our content

    # Render menu bar
    pygame.draw.rect(screen,black,(-1, -1, 1280, 16), 1)
    draw_text((8,0),"| Operations | <F1> load file | <F2> start | F3 pause | F4 move one step | F5 show help |                      | CHIP ARBITRARY MADE BY RE | VERSION 1.1 |")
    #"                     | CHIP ARBITRARY MADE BY RE | VERSION 1.1 |"

    # Render status bar
    pygame.draw.rect(screen,black,(-1, 700-8, 1280, 24), 1)

    draw_text((4,700-4),"Instructions Per Second: "+str(instructions_per_second)+" | Virtual CPU Usage: "+core_activity)

    # Emulator State
    draw_text((grid_pos(0,0)[0],(grid_pos(0,0)[1]+8)),"VM State")
    if vm_state==0:
        render_button(grid_pos(0,1), False, "Ready")
    elif vm_state==1:
        render_button(grid_pos(0,1), False, "Running")
    elif vm_state==2:
        render_button(grid_pos(0,1), False, "Halted")
    elif vm_state==3:
        render_button(grid_pos(0,1), True, "Crashed")

    render_button(grid_pos(0,2), False, "N/A")
    render_button(grid_pos(0,3), False, "N/A")
    render_button(grid_pos(0,4), False, "N/A")
    render_button(grid_pos(0,5), False, "N/A")

    render_button(grid_pos(0,6), False, "CUR: "+str(console_buffer[c_x,c_y]))

    # Details
    render_button(grid_pos(0,16), False, "IP: "+str(registers[32])) # Instruction Pointer is R32
    render_button(grid_pos(1,16), False, "Cap: "+str(execution_wait)+"ms")
    render_button(grid_pos(2,16), False, "V_MODE: "+str(v_mode))
    render_button(grid_pos(3,16), False, "C_X: "+str(c_x))
    render_button(grid_pos(4,16), False, "C_Y: "+str(c_y))
    render_button(grid_pos(5,16), False, "C_F: "+str(c_f))


    draw_text((grid_pos(9,0)[0],(grid_pos(0,0)[1]+8)),"General Purpose Registers")
    #Render registers
    for x in range(0,16):
        if not x%2:
            render_button(grid_pos(9,1+x), False, "G"+str(hex(x)[-1:]).upper()+": "+str(registers[x]),(207,207,207))
        else:
            render_button(grid_pos(9,1+x), False, "G"+str(hex(x)[-1:]).upper()+": "+str(registers[x]),(245,245,245))
    for x in range(0,16):
        if not x%2:
            render_button(grid_pos(10,1+x), False, "I"+str(hex(x)[-1:]).upper()+": "+str(registers[x+16]),(207,207,207))
        else:
            render_button(grid_pos(10,1+x), False, "I"+str(hex(x)[-1:]).upper()+": "+str(registers[x+16]),(245,245,245))

    # Render console box
    draw_text((grid_pos(0,0)[0],(grid_pos(0,7)[1]+16+4)),"Virtual Display")
    pygame.draw.rect(screen,black,(grid_pos(0,0)[0], (grid_pos(0,8)[1]), 654+16, 295+16), 0)

    for x in range(0,82):
        for y in range(0, 25):
            cur_char = int(console_buffer[x,y]) % 255
            if x == c_x and y == c_y:
                if c_f == 1:
                        if tick2:
                            draw_text(((grid_pos(0,8)[0]+2)+9+(x*8),grid_pos(0,8)[1]+9+(y*12)), chr(cur_char), white);
                else:
                    draw_text(((grid_pos(0,8)[0]+2)+9+(x*8),grid_pos(0,8)[1]+9+(y*12)), chr(cur_char), white);

            else:
                draw_text(((grid_pos(0,8)[0]+2)+9+(x*8),grid_pos(0,8)[1]+9+(y*12)), chr(cur_char), white);

    # Render Instructions Box
    draw_text((grid_pos(6,0)[0]+16,(grid_pos(6,0)[1]+8)),"Event/instruction Log")
    pygame.draw.rect(screen,black,(grid_pos(6,0)[0]+16, (grid_pos(0,1)[1]), 300, 272+64+16), 1)

    for x in range(0,21):

        if not x%2:
            pygame.draw.rect(screen,(207,207,207),(grid_pos(6,0)[0]+16+1,(grid_pos(6,1)[1])+(x*16)+8, 298, 14), 0)
        else:
            pygame.draw.rect(screen,(245,245,245),(grid_pos(6,0)[0]+16+1,(grid_pos(6,1)[1])+(x*16)+8, 298, 14), 0)

        draw_text((grid_pos(6,0)[0]+16+8,(grid_pos(6,1)[1])+(x*16)+8),str(instruction_buffer[x]))

        #"["+str(x)+"]"+

    # Render serial recieved
    draw_text((grid_pos(6,0)[0]+16,(grid_pos(6,10)[1])+8),"Console Output (ASCII)")
    pygame.draw.rect(screen,black,(grid_pos(6,0)[0]+16, (grid_pos(0,11)[1]), 300, 200+24), 1)

    for x in range(0,18):
        for y in range(18):
            n = serial_buffer.reshape(18,18)
            draw_text((grid_pos(6,0)[0]+(y*16)+16+8,(grid_pos(6,11)[1])+(x*12)+4),chr(int(n[x,y])))
        #"["+str(x)+"]"+

    # Render KBD Event Stack
    draw_text((grid_pos(5,0)[0]+32+8,(grid_pos(0,0)[1])+8),"KBD Buffer")
    pygame.draw.rect(screen,black,(grid_pos(5,0)[0]+64, (grid_pos(0,1)[1]), 8+32+8, 254), 1)
    for x in range(len(kbd_events)):
        pygame.draw.rect(screen, black,(grid_pos(5,0)[0]+64+8,(grid_pos(0,1)[1])+(x*16)+8,32,14),1)
        try:
            draw_text((grid_pos(5,0)[0]+64+8+4,(grid_pos(0,1)[1])+(x*16)+8),str(kbd_events[x]))
        except:
            pass # herp a derp thread deleted ur event
        if x==14: break

    # Render relevant memory
    draw_text((grid_pos(1,0)[0],(grid_pos(0,0)[1])+8),"Relevant Memory (Near Instruction Pointer)")
    pygame.draw.rect(screen,black,(grid_pos(1,0)[0], (grid_pos(0,1)[1]), 432+64, 254), 1)

    incrementer = 0
    for x in range(int(registers[32]-9),int(registers[32]+9)):
        if not incrementer%2:
            pygame.draw.rect(screen,(207,207,207),(grid_pos(1,0)[0]+1,(grid_pos(0,1)[1])+8+(incrementer*13), 432+62, 14), 0)
        else:
            pygame.draw.rect(screen,(247,247,247),(grid_pos(1,0)[0]+1,(grid_pos(0,1)[1])+8+(incrementer*13), 432+62, 14), 0)

        if incrementer == 9:
            pygame.draw.rect(screen,red,(grid_pos(1,0)[0]+1,(grid_pos(0,1)[1]+1)+8+(incrementer*13), 432+62, 14), 0)

        draw_text((grid_pos(1,0)[0]+8,(grid_pos(0,1)[1])+8+(incrementer*13)),"["+str(x%(1024*16))+"] "+str(memory[x]))
        incrementer += 1

    # Update the screen.
    pygame.display.update()

def process_events():
    global vm_state, run_threads
    # F1-F4
    # 282-285
    # "<F1> load file | <F2> start | F3 pause | F4 move one step"
    # Process all events
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.KEYDOWN: # KBD event?

            if event.key == 282:
                vm_state = 0 # Stop the core execution
                load_program()
            elif event.key == 283:
                vm_state = 1
            elif event.key == 284:
                vm_state = 0
            elif event.key == 285:
                execution_tick()
            elif event.key == 286:
                show_help()
            elif vm_state==1: # Is VM running?
                kbd_event_push(event.key) # Push KBD event to stack

        if event.type == pygame.QUIT:
            run_threads = 0


def process_ticks():
    global tick0_lasttick, tick1_lasttick, tick2_lasttick, tick0, tick1, tick2
    # Process  ticks
    if tick0_lasttick < int(time.time()): # Full tick
        tick0 = not tick0
        tick0_lasttick = int(time.time())

    if tick1_lasttick < (time.time()-0.5): # Half Tick
        tick1 = not tick1
        tick1_lasttick = time.time()

    if tick2_lasttick < (time.time()-0.25): # Quarterly Tick
        tick2 = not tick2
        tick2_lasttick = time.time()

def execution_tick():
    global vm_state, c_x, c_y, c_f, v_mode, console_buffer

    cur_inst = memory[registers[32]]

    # VM INTERFACING
    if cur_inst == 255: # Halt Instruction
        log_add("["+str(int(registers[32]))+"] HLT")
        vm_state = 2 # Specify it was haulted

    elif cur_inst == 208: # Sleep
        log_add("["+str(int(registers[32]))+"] SLEEP "+str(memory[registers[32]+1]))
        time.sleep(registers[int(memory[registers[32]+1])]/1000)
        registers[32] = registers[32] + 2

    # SERIAL CONSOLE
    elif cur_inst == 254: # Print Char
        log_add("["+str(int(registers[32]))+"] PRT_C R"+str(int(memory[registers[32]+1])))

        register_containing_char = int(memory[registers[32]+1])

        # Do whatever to print here
        serial_add( chr(int(registers[register_containing_char])))

        # Increment Instruction Pointer
        registers[32] = registers[32] + 2

     # INSTRUCTION
    elif cur_inst == 225: # Print Number
        log_add("["+str(int(registers[32]))+"] PRT_N R"+str(int(memory[registers[32]+1])))

        register_containing_no = memory[registers[32]+1]

        # Print our number out, one digit at a time.

        # Convert our number to a processable string. Also remove any decimal point
        output_string = str(int(registers[int(register_containing_no)]))
        # Loop through all chars in string
        for x in range(len(str(output_string))):
            serial_add(output_string[x])


        # Increment Instruction Pointer
        registers[32] = registers[32] + 2

     # INSTRUCTION
    elif cur_inst == 237: # Set Register
        log_add("["+str(int(registers[32]))+"] SET R"+str(int(memory[registers[32]+1]))+" "+str(memory[registers[32]+2]))
        registers[int(memory[registers[32]+1])] = memory[registers[32]+2]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 3

     # INSTRUCTION
    elif cur_inst == 236: # Exchange Register
        log_add("["+str(int(registers[32]))+"] XCH "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))

        # Exchange Data Betwen Registers
        register0_content = registers[int(memory[registers[32]+1])]
        register1_content = registers[int(memory[registers[32]+2])]

        registers[int(memory[registers[32]+2])] = register0_content

        registers[int(memory[registers[32]+1])] = register1_content

        # Increment Instruction Pointer
        registers[32] = registers[32] + 3

     # INSTRUCTION
    elif cur_inst == 235: # Copy Register
        log_add("["+str(int(registers[32]))+"] MOV "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))

        # Exchange Data Betwen Registers
        registers[int(memory[registers[32]+2])] = registers[int(memory[registers[32]+1])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 3

    # INSTRUCTION
    elif cur_inst == 234: # Move data from register to memory position specified by register
        log_add("["+str(int(registers[32]))+"] MOVR "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))

        # Get Register Data
        register0_content = registers[int(memory[registers[32]+1])]
        register1_content = registers[int(memory[registers[32]+2])]

        # Now move contents of register0 to memory position specified by contents of register1
        memory[register1_content] = register0_content

        # Increment Instruction Pointer
        registers[32] = registers[32] + 3

    # INSTRUCTION
    elif cur_inst == 233: # Move data from MEMORY POSITION specified by R1 to R0
        log_add("["+str(int(registers[32]))+"] MOVM "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))

        # Get Register Data
        register0_content = registers[int(memory[registers[32]+1])]
        register1_content = registers[int(memory[registers[32]+2])]

        # Now move contents of the memory position specified by REGISTER1 to REGISTER0
        registers[int(memory[registers[32]+1])] = memory[register1_content]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 3

     # INSTRUCTION
    elif cur_inst == 232: # ADD
        log_add("["+str(int(registers[32]))+"] ADD "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] + registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4

    # INSTRUCTION
    elif cur_inst == 231: # SUB
        log_add("["+str(int(registers[32]))+"] SUB "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] - registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4

    # INSTRUCTION
    elif cur_inst == 230: # MUL
        log_add("["+str(int(registers[32]))+"] MUL "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] * registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4

    elif cur_inst == 229: # DIV
        log_add("["+str(int(registers[32]))+"] DIV "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] / registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4

    elif cur_inst == 228: # AND
        log_add("["+str(int(registers[32]))+"] AND "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] & registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4

    elif cur_inst == 226: # OR
        log_add("["+str(int(registers[32]))+"] OR "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] | registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4
    elif cur_inst == 210: # XOR
        log_add("["+str(int(registers[32]))+"]XOR "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] ^ registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4
    elif cur_inst == 209: # MOD
        log_add("["+str(int(registers[32]))+"] MOD "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+3])] = registers[int(memory[registers[32]+1])] % registers[int(memory[registers[32]+2])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4
    elif cur_inst == 227: # not
        log_add("["+str(int(registers[32]))+"] NOT "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))

        # Add register0 to register1 and store in register3
        registers[int(memory[registers[32]+2])] = not registers[int(memory[registers[32]+1])]

        # Increment Instruction Pointer
        registers[32] = registers[32] + 4

    elif cur_inst == 252: # JUMP ABSOLUTE
        log_add("["+str(int(registers[32]))+"] JMP ")
        # Jump to the desired position
        registers[32] = registers[15]
    elif cur_inst == 250: # JUMP ABSOLUTE MATCH
        log_add("["+str(int(registers[32]))+"] JME "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))
        if registers[int(memory[registers[32]+1])] == registers[int(memory[registers[32]+2])]:
            registers[32] = registers[15]
        else:
            registers[32] = registers[32] + 3
    elif cur_inst == 249: # JUMP ABSOLUTE NOT MATCH
        log_add("["+str(int(registers[32]))+"] JMD "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))
        if registers[int(memory[registers[32]+1])] != registers[int(memory[registers[32]+2])]:
            registers[32] = registers[15]
        else:
            registers[32] = registers[32] + 3
    elif cur_inst == 248: # JUMP ABSOLUTE BIGGER
        log_add("["+str(int(registers[32]))+"] JMB "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))
        if registers[int(memory[registers[32]+1])] > registers[int(memory[registers[32]+2])]:
            registers[32] = registers[15]
        else:
            registers[32] = registers[32] + 3
    elif cur_inst == 247: # JUMP ABSOLUTE SMALLER
        log_add("["+str(int(registers[32]))+"] JMS "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))
        if registers[int(memory[registers[32]+1])] < registers[int(memory[registers[32]+2])]:
            registers[32] = registers[15]
        else:
            registers[32] = registers[32] + 3
    elif cur_inst == 246: # JUMP ABSOLUTE BIGGER OR EQUAL
        log_add("["+str(int(registers[32]))+"] JMN "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))
        if registers[int(memory[registers[32]+1])] >= registers[int(memory[registers[32]+2])]:
            registers[32] = registers[15]
        else:
            registers[32] = registers[32] + 3
    elif cur_inst == 245: # JUMP ABSOLUTE SMALLER OR EQUAL
        log_add("["+str(int(registers[32]))+"] JMM "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))
        if registers[int(memory[registers[32]+1])] <= registers[int(memory[registers[32]+2])]:
            registers[32] = registers[15]
        else:
            registers[32] = registers[32] + 3

    # RELATIVE JUMPS HERE
    elif cur_inst == 244: # JUMP RELATIVE
        log_add("["+str(int(registers[32]))+"] JMP_R "+str(memory[registers[32]+1]))
        # Jump to the desired position
        registers[32] =  registers[32] + int(memory[registers[32]+1])
    elif cur_inst == 243: # JUMP RELATIVE MATCH
        log_add("["+str(int(registers[32]))+"] JME_R "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))
        if registers[int(memory[registers[32]+2])] == registers[int(memory[registers[32]+3])]:
            registers[32] = registers[32] + registers[int(memory[registers[32]+1])]
        else:
            registers[32] = registers[32] + 4
    elif cur_inst == 242: # JUMP RELATIVE NOT MATCH
        log_add("["+str(int(registers[32]))+"] JMD_R "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))
        if registers[int(memory[registers[32]+2])] != registers[int(memory[registers[32]+3])]:
            registers[32] = registers[32] + registers[int(memory[registers[32]+1])]
        else:
            registers[32] = registers[32] + 4
    elif cur_inst == 241: # JUMP RELATIVE BIGGER
        log_add("["+str(int(registers[32]))+"] JMB_R "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))
        if registers[int(memory[registers[32]+2])] > registers[int(memory[registers[32]+3])]:
            registers[32] = registers[32] + registers[int(memory[registers[32]+1])]
        else:
            registers[32] = registers[32] + 4
    elif cur_inst == 240: # JUMP RELATIVE SMALLER
        log_add("["+str(int(registers[32]))+"] JMS_R "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))
        if registers[int(memory[registers[32]+2])] < registers[int(memory[registers[32]+3])]:
            registers[32] = registers[32] + registers[int(memory[registers[32]+1])]
        else:
            registers[32] = registers[32] + 4
    elif cur_inst == 239: # JUMP RELATIVE BIGGER OR EQUAL
        log_add("["+str(int(registers[32]))+"] JMN_R "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))
        if registers[int(memory[registers[32]+2])] >= registers[int(memory[registers[32]+3])]:
            registers[32] = registers[32] + registers[int(memory[registers[32]+1])]
        else:
            registers[32] = registers[32] + 4
    elif cur_inst == 238: # JUMP RELATIVE SMALLER OR EQUAL
        log_add("["+str(int(registers[32]))+"] JMM_R "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2])+" "+str(memory[registers[32]+3]))
        if registers[int(memory[registers[32]+2])] <= registers[int(memory[registers[32]+3])]:
            registers[32] = registers[32] + registers[int(memory[registers[32]+1])]
        else:
            registers[32] = registers[32] + 4

    # DISPLAY INTERFACING INSTRUCTIONS
    elif cur_inst == 224: # SET VIDEO MODE
        log_add("["+str(int(registers[32]))+"] SET_VMODE "+str(memory[registers[32]+1]))
        v_mode = registers[int(memory[registers[32]+1])]
        registers[32] = registers[32] + 2
    elif cur_inst == 223: # SET CURSOR X
        log_add("["+str(int(registers[32]))+"] SET_CX "+str(memory[registers[32]+1]))
        c_x = registers[int(memory[registers[32]+1])] % 82
        registers[32] = registers[32] + 2
    elif cur_inst == 222: # SET CURSOR Y
        log_add("["+str(int(registers[32]))+"] SET_CY "+str(memory[registers[32]+1]))
        c_y = registers[int(memory[registers[32]+1])] % 25
        registers[32] = registers[32] + 2
    elif cur_inst == 221: # SET CURSOR FORMAT
        log_add("["+str(int(registers[32]))+"] SET_CF "+str(memory[registers[32]+1]))
        c_f = registers[int(memory[registers[32]+1])]
        registers[32] = registers[32] + 2
    elif cur_inst == 220: # SET CURSOR COnTENTS
        log_add("["+str(int(registers[32]))+"] SET_CUR "+str(memory[registers[32]+1]))
        console_buffer[c_x,c_y] = registers[int(memory[registers[32]+1])]
        registers[32] = registers[32] + 2
    elif cur_inst == 212: # GET CURSOR CONTENTS
        log_add("["+str(int(registers[32]))+"] GET_CUR "+str(memory[registers[32]+1]))
        registers[int(memory[registers[32]+1])] = console_buffer[c_x,c_y]
        registers[32] = registers[32] + 2
    elif cur_inst == 213: # CLEAR SCREEN CONTENTS
        log_add("["+str(int(registers[32]))+"] SCR_CLR ")
        console_buffer = numpy.zeros(82*25).reshape(82,25)
        registers[32] += 1
    elif cur_inst == 211: # SCREEN SHIFT
        log_add("["+str(int(registers[32]))+"] SCR_SHIFT "+str(memory[registers[32]+1])+" "+str(memory[registers[32]+2]))
        #registers[int(memory[registers[32]+1])]
        # 0=HORIZONTAL, 1=VERTICAL
        console_buffer = numpy.roll(console_buffer,1,axis=2)
        registers[32] += 2

    # KEYBOARD INTERFACING INSTRUCTIONS
    elif cur_inst == 219: # KB BUFFER POP
        log_add("["+str(int(registers[32]))+"] KB POP "+str(memory[registers[32]+1]))
        registers[int(memory[registers[32]+1])] = kbd_event_pop()
        registers[32] = registers[32] + 2

    # INCR/DECR
    elif cur_inst == 217: # increment register
        log_add("["+str(int(registers[32]))+"] INCR "+str(memory[registers[32]+1]))
        registers[int(memory[registers[32]+1])] = registers[int(memory[registers[32]+1])] + 1
        registers[32] = registers[32] + 2
    elif cur_inst == 216: # decrement register
        log_add("["+str(int(registers[32]))+"] DECR "+str(memory[registers[32]+1]))
        registers[int(memory[registers[32]+1])] = registers[int(memory[registers[32]+1])] - 1
        registers[32] = registers[32] + 2

    # INTERRUPTS/FUNCTIONS
    elif cur_inst == 215: #NEW: interrupt
        log_add("["+str(int(registers[32]))+"] INT "+str(memory[registers[32]+1]))
        # Interrupt function
        # Remember our next IP for the interrupt to return in R31
        registers[31] = registers[32] + 2

        # Then Jump to our interrupt
        registers[32] = memory[registers[32]+1]
    elif cur_inst == 214: #NEW: ret (return to where we were before interrupt)
        log_add("["+str(int(registers[32]))+"] RET")
        # Just return where we were
        registers[32] = registers[31]
    # INSTRUCTION
    else:
        log_add("["+str(int(registers[32]))+"] Invalid Opcode "+str(memory[registers[32]]))
        vm_state = 3 # Specify error

instructions_per_second = 0

execution_wait = 0 # Speed cap for the execution thread in MS
instruction_count = 0


core_activity = ""

def execution_thread():
    global run_threads, vm_state, instructions_per_second, instruction_count

    # For internal (Speed recording) use only
    start_time = 0

    while 1:
        # Check if main thread is requesting stop
        if run_threads == 0:
            break # End the loop

        # Process our current instruction if the VM is running
        if vm_state == 1:

            try:
                if start_time != int(time.time()):
                    start_time = int(time.time())
                    instructions_per_second = instruction_count
                    instruction_count = 0
                execution_tick() # Will fail if u dun broke something :D
                time.sleep(execution_wait/1000)
                instruction_count +=1
            except Exception,e:
                log_add("HALT: Core Exception. ")
                log_add(e)
                vm_state = 3
        else:
            time.sleep(0.1)

def cpu_usage_calculation_thread():
    global instruction_count, core_activity

    # CPU Usage calculation
    last_ip = 0
    activity_checks = 0
    activity_ticks = 0
    start_time2 = 0
    while 1:
        # Determined by: In the past X checks, CPU ticked Y Times, Usage is the percentage of Y in X
        activity_checks +=1
        if instruction_count != last_ip:
                last_ip = instruction_count
                activity_ticks += 1

        if start_time2 != int(time.time()): # Runs only once a second
            start_time2 = int(time.time())
            core_activity = str(int((activity_ticks/float(activity_checks))*100.0))+"%"+" ("+str(activity_ticks)+"/"+str(activity_checks)+")"
            # Reset counters
            activity_checks = 0
            activity_ticks = 0
        time.sleep(0.001)


#log_add("TEST "+str(random.random()))
#serial_add(random.randint(0,255))
#console_buffer[random.randint(0,81),random.randint(0,24)] = random.randint(0,255)
def show_help():
    log_add("Welcome to CHIP ARBITRARY")
    log_add("CHIP ARBITRARY was made by Re")
    log_add("")
    log_add("At the right most, We have all")
    log_add("of our registers & their content.")
    log_add("")
    log_add("The 'Console Output' box under ")
    log_add("is the 'virtual serial port' and")
    log_add("may be used for debugging purposes.")
    log_add("")
    log_add("Al the left most, we have the")
    log_add("'Virtual Display', a monochrome")
    log_add("82x25 ASCII canvas.")
    log_add("")
    log_add("Under the 'Virtual Display' is")
    log_add("some more information.")
    log_add("")
    log_add("IP = Instruction Pointer (R32)")
    log_add("IP/s = Instructions Per Second (FPS)")
    log_add("V_MODE = Video Mode (Text, Raster)")
    log_add("C_X, C_y = Cursor Positions")
    log_add("C_F = Cursor Form (Constant/Blinking)")

show_help()

run_threads = 1

import threading
thread = threading.Thread(target=execution_thread)
thread.start()

import threading
thread = threading.Thread(target=cpu_usage_calculation_thread)
thread.start()


while (1):
    if run_threads == 0:
        time.sleep(0.2)
        pygame.quit()
        sys.exit()
    # Process all Pygame events first, incl kbd input
    process_events()

    # Make all tickers tick as they should
    process_ticks()

    # Render our GUI
    render_gui()

    # Limit our FPS
    clock.tick(30)