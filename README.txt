CHIP ARBITRARY
Made by Re
Version 1.1

DESCRIPTION:
When re was 14, he found CHIP8, and he loved it. He tried to make his own version of CHIP8. 
He failed. 
He tried again at age 16. 
He failed. 
Now he tries again.

CHIP8 is an interpreted programming language for programming games that ran in a VM.
Allowing games to be programmed once and then run on multiple different types of computers.

CHIP ARBITRARY is like CHIP8, except not limited to 8-Bits and is more general purpose.

REQUIREMENTS:
	Python 2.7.2
	NumPy 1.8+ (Data handling)
	(Only for MAIN.PYW) PyGame 1.9.1+ (GUI)

VIRTUAL MACHINE:

The VM has 33 registers, First 16 are general purpose, Second 16 are for interrupt use (Subroutine?)
REGISTERS:
	Recommended for General Purpose (Primary): 
		G0-GF
		G0 - 0
		G1 - 1
		G2 - 2
		G3 - 3
		G4 - 4
		G5 - 5
		G6 - 6
		G7 - 7
		G8 - 8
		G9 - 9
		GA - 10
		GB - 11
		GC - 12
		GD - 13
		GE - 14
		(RESERVED)GF - 15
		(Jumping)
			15 = GF = JM = Register for where to jump to
	Recommended For Interrupt Use Only (Secondary):
		I0-IF
		I0 - 16
		I1 - 17
		I2 - 18
		I3 - 19
		I4 - 20
		I5 - 21
		I6 - 22
		I7 - 23
		I8 - 24
		I9 - 25
		IA - 26
		IB - 27
		IC - 28
		ID - 29
		IE - 30
		(RESERVED FOR RET) IF - RET - 31
		(Interrupts Details)
			(RESERVED if interrupts used) IF = RET = 31 = Return address for interrupt
	Special (Any):
		(General)
			IP = 32 = Instruction Pointer
		
The VM has a memory array of 1024*16 values. Each value may be any number.
Memory is for instructions and their data.

The VM also has a Keyboard Buffer, On each key press the key is added to the end. 
KB_POP gets the first item in the buffer and removes it.

Instructions are all variable length, and are as follows:
OPCODE PARAM PARAM ... PARAM

Instruction Set Groups:
	VM Interfacing: Instructions that interact with the VM's Execution
	Bios: Instructions for invoking bios interrupts
	Console Interfacing: Instructions for outputting text and numbers to the 'Virtual Serial Port' (Typically for debugging code)
	Display Interfacing: Instructions for using the 'Virtual Display'
	Keyboard Interfacing: Instructions for getting Keyboard Input
	Register/Memory: Instructions for interacting with registers and memory
	Jumps: Instructions for changing the instruction pointer (Conditionally or unconditionally)
	Arithmetic: Instructions for doing arithmetic to register data


Instruction Set Details:
	LEGEND:
		RX = Register (R0-R31 or IP)
		V = Direct value
		
	VM Interfacing:
		HLT - (1)255 - Halts execution
		SLEEP - (2)208, RX - Sleeps RX miliseconds 
	Interrupts:
		INT - (2)215, V - Begins interrupt V and remembers where our IP was to return upon calling RET
		RET - (1)214 - Returns to where we should go after calling INT
	Console Interfacing:
		PRT_C - (2)254 RX - Outputs contents of RX to 'Virtual Serial Port' (Displayed as ASCII)
		PRT_n - (2) 225 RX - Prints all digits of a number contained by RX to 'Virtual Serial Port'
	Display Interfacing:
		SET_VMODE - (2)224, RX - Sets the Virtual Display Mode (Currently, Only Monochrome Text)
		SET_CX - (2)223, RX - Sets the cursor X position to contents of RX
		SET_CY - (2)222, RX - Sets the cursor Y position to contents of RX
		SET_CF - (2)221, RX - Sets the cursor format (ON or BLINKING) to contents of RX
		SET_CUR - (2)220 , RX - Sets the cursor's value to contents of RX
		GET_CUR - (2)212, RX - Sets RX to the cursor's value
		SCR_SHIFT - (2) 211, RX, RX0 - Shifts screen contents by RX in axis RX0 (NUMPY.ROLL)
		SCR_CLR - (1)213 - Clears the screen
	Keyboard Interfacing:
		KB_POP - (2)219, RX - Sets RX to the first item in the KBD buffer and removes it from the buffer
	Jumps:
		Absolute:
			JMP - (1)252 - Jumps to position contained in JM (PRE-DEFINE)
			JME - (2)250, RX1, RX2 - Jumps to position contained in JM (PRE-DEFINE) (IF RX1 == RX2)
			JMD - (2)249, RX1, RX2 - Jumps to position contained in JM (PRE-DEFINE) (IF RX1 != RX2)
			JMB - (2)248, RX1, RX2 - Jumps to position contained in JM (PRE-DEFINE) (IF RX1 > RX2)
			JMS - (2)247, RX1, RX2 - Jumps to position contained in JM (PRE-DEFINE) (IF RX1 < RX2)
			JMN - (2)246, RX1, RX2 - Jumps to position contained in JM (PRE-DEFINE) (IF RX1 >= RX2)
			JMM - (2)245, RX1, RX2 - Jumps to position contained in JM (PRE-DEFINE) (IF RX1 <= RX2)
		Relative: (ALL OUTDATED, DONT USE)
			JMP_R - (2)244, RX - Jumps RX positions (Positive or Negative)
			JME - (2)250, RX, RX1, RX2 - Jumps RX positions (IF RX1 == RX2)
			JMD - (2)249, RX, RX1, RX2 - Jumps RX positions (IF RX1 != RX2)
			JMB - (2)248, RX, RX1, RX2 - Jumps RX positions (IF RX1 > RX2)
			JMS - (2)247, RX, RX1, RX2 - Jumps RX positions (IF RX1 < RX2)
			JMN - (2)246, RX, RX1, RX2 - Jumps RX positions (IF RX1 >= RX2)
			JMM - (2)245, RX, RX1, RX2 - Jumps RX positions (IF RX1 <= RX2)
		
	Register/Memory:
		SET - (3) 237, RX, V - Sets RX to value V
		XCH - (3) 236, RX, RX1 - Exchange values of RX and RX1
		MOV - (3)235, RX, RX1 - Copy value of RX to RX1
		MOVR - (3)234, RX, RX1 - Set memory at position RX1 to value of RX
		MOVM - (3)233, RX, RX1 - Set RX to value of memory at at position RX1
	Arithmetic:
		INCR - (2)217, RX - Increments RX by 1
		DEC - (2)216, RX - Decrement RX by 1
		ADD - (4)232 RX, RX1, RX2 - Adds RX to RX1 and stores result in RX2
		SUB - (4)231 RX, RX1, RX2 - Subtracts RX from RX1 and stores result in RX2
		MUL - (4)230 RX, RX1, RX2 - Multiplies RX with RX1 and stores result in RX2
		DIV - (4)229 RX, RX1, RX2 - Divides RX by RX1 and stores result in RX2
		AND - (4)228 RX, RX1, RX2 - RX AND RX1 and stores result in RX2
		OR - (4)227 RX, RX1, RX2 - RX OR RX1 and stores result in RX2
		NOT - (4)226 RX, RX1 - NOT RX and stores result in RX1
		XOR - (4)210 RX, RX1, RX2 - RX XOR RX1 and stores result in RX2
		MOD - (4)209 RX, RX1, RX2 - RX % RX1 and stores result in RX2
		
	